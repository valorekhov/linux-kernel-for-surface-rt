# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

pkgbase=linux-surface-rt-efi
pkgver=5.14.9
pkgrel=1
pkgdesc='Linux'
_srctag=v${pkgver%.*}-${pkgver##*.}
url="https://github.com/archlinux/linux/commits/$_srctag"
arch=(armv7h)
license=(GPL2)
makedepends=(
  bc kmod libelf pahole cpio perl tar xz
  xmlto python-sphinx python-sphinx_rtd_theme graphviz imagemagick
  git
)
options=('!strip')
#_srcname=archlinux-linux
source=(
  "https://www.kernel.org/pub/linux/kernel/v5.x/linux-$pkgver.tar".{xz,sign}
  config.surface-rt.efi.defaults          #  kernel config file delta from 'make tegra_defconfig'
  #Device tree
  #"https://raw.githubusercontent.com/grate-driver/linux/master/arch/arm/boot/dts/tegra30-microsoft-surface-rt-efi.dts"
  tegra30-microsoft-surface-rt-efi.dts
  #"https://raw.githubusercontent.com/grate-driver/linux/master/arch/arm/boot/dts/tegra30-microsoft-surface-rt.dts"
  tegra30-microsoft-surface-rt.dts
  #"https://raw.githubusercontent.com/grate-driver/linux/master/arch/arm/boot/dts/tegra30.dtsi"
  tegra30.dtsi
  #"https://raw.githubusercontent.com/grate-driver/linux/master/arch/arm/boot/dts/tegra30-peripherals-opp.dtsi"
  tegra30-peripherals-opp.dtsi
  0000-arch-arm-boot-dts-Makefile.patch
  # ACPI Parking Protocol Patch
  0001-Add-ACPI-Parking-Protocol-for-arm32-devices.patch
  #tCover Patch
  0001-WIP-ARM-tegra-Add-Type-Touch-Power-Cover-support-for.patch
  # hotplug Patch for tCover
  0001-i2c-GPIO-based-hotplug-gate.patch
)
validpgpkeys=(
  'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # Linus Torvalds
  '647F28654894E3BD457199BE38DBBDC86092693E'  # Greg Kroah-Hartman
)
sha256sums=('ba8f07db92d514a2636e882bcd646f79f1c8ab83f5ad82910732dd0ec83c87e6'
            'SKIP'
            '41ef9b14fb3c9aaeb52c197a942693012dde47afaac29bcb6e47c719f7b6af73'
            '7d2f9a0d50133c464d4e5a0921400c328730f8f7b1f181aafa089f2f832d30bd'
            '21ac113a4343a85c3e538c62d851cf4f50c2c9679a0883765fcf69cc9f830597'
            '436ac18da0a79af91b4292c5b35173e4d3f253b4f7f30b44fb5a5f717f07d8e6'
            'a52485a800e143fe2b2c73c9d7651e95859ffa9b776838820f70809b229902d4'
            'c2ac853afeda979240983bf8f4e62c31f114a08469037ce1f09f9b7f72f040b4'
            '279d64e374982c2d1c25d315864cae9ce458b52f3d3a125f5cebd85a86899982'
            'acd17d42442fd4bac8d89faa803268134542648c08185a9502d4cef94900cd63'
            'c499399fa953f3bdb003f00e94745ab7f4df60f18d5ee5027825f9692d8192ba')
            
export KBUILD_BUILD_HOST=archlinuxarm
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
  cd linux-${pkgver}

  echo "Setting version..."
  scripts/setlocalversion --save-scmversion
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux}" > localversion.20-pkgname

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done

  echo "Setting config..."

  #make olddefconfig
  make ARCH=arm tegra_defconfig
  sed -i 's/CONFIG_DVB_CORE=y/CONFIG_DVB_CORE=n/' .config
  sed -i 's/CONFIG_MEDIA_TUNER=y/CONFIG_MEDIA_TUNER=n/' .config
  sed -i 's/CONFIG_MEDIA_DIGITAL_TV_SUPPORT=y/CONFIG_MEDIA_DIGITAL_TV_SUPPORT=n/' .config
  cat ../config.surface-rt.efi.defaults >> .config

  cp ../tegra30*.dts* ./arch/arm/boot/dts/

  make -s kernelrelease > version
  echo "Prepared $pkgbase version $(<version)"
}

build() {
  cd linux-${pkgver}
  make all ARCH=arm -j $(nproc)
  # append device tree to the kernel
  # cat ./arch/arm/boot/zImage ./arch/arm/boot/dts/tegra30-microsoft-surface-rt-efi.dtb > ./arch/arm/boot/zImage
#  make htmldocs
}

_package() {
  pkgdesc="The $pkgdesc kernel and modules"
  depends=(coreutils kmod initramfs)
  optdepends=('crda: to set the correct wireless channels of your country'
              'linux-firmware: firmware images needed for some devices')
  #provides=(VIRTUALBOX-GUEST-MODULES WIREGUARD-MODULE)
  #replaces=(virtualbox-guest-modules-arch wireguard-arch)

  cd linux-${pkgver}
  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 modules_install

  # remove build and source links
  rm "$modulesdir"/{source,build}
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)

  cd linux-${pkgver}
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/arm" -m644 arch/arm/Makefile
  cp -t "$builddir" -a scripts

  # add objtool for external module building and enabled VALIDATION_STACK option
  # install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # add xfs and shmem for aufs building
  mkdir -p "$builddir"/{fs/xfs,mm}

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/arm" -a arch/arm/include
  install -Dt "$builddir/arch/arm/kernel" -m644 arch/arm/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */arm/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

pkgname=("$pkgbase" "$pkgbase-headers")
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
